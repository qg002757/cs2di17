# CS2DI17
___
# Information security
Here we will look at how you might attack a computer system over a network.
We will lokk at how network are attacked
We will look at how an attack would work, the methodolody, the goals and reconnaissance
# Methodology
The methodology an attacker might use could be:
1. Footprint
2. Scan
3. Enumerate
4. Penetrate
5. Attack
6. Cover tracks
7. Install backdoors

# Reconnaissance
Now steps 1-3 are reconnaissance

# Footprinting
This is the process of information gathering about the target. One can do research and find:
1. Technical information
- IP addresses
- Web presence (Websites, e-mail servers)
- Recent security event
2. or they can look for
- Contact details
- Phone numbers

During footprint, the questions one might ask include:
1. What software is the target based on
- Libraries, server software, other programs
2. What languages does the target use
- How pup to date are they
3. How can you communicate with the target
- HTTP? E-mail? Telnet?
4. Who is associated with the target
- Webmaster? Business ownner

# Footprinting tools
There are several tools available for example there are tools that:
1. Tell you information about the server
2. Many web tools such as:
- who.is
- w3dt.net
3. Also OS based tools
- nslookup
Attackers usually try out multiple tools

Social engineering is a big one, so this include using:
- Linkedin, Facebook and etc to get information on staff
- Dumpster diving: stealing old info from rubbish bins
Attacker could also get e-mails and personal information for a spear phising attack

# Scanning
Determine whihc of the systems are accessible from the internet
- Whihc IP addresses are accessible
- What software are running
- Any obviously open doors
At its most basic:
- Ping the address to see if it is alive
- Scan the ports to see if any are open
- Identifies open port
- Google the port number to get the use/applicaiton of that port
- Goole the application to get known vunerabilities
- More on the port numbers:
+ 1 - 1023 are system - used by the OS
+ 1024 - 49151 are registered for a specific purpose
+ 49152 - 65535 are custom - Used for what ever the user wants

# Scanning: nmap output
An example of Scanning using the nmap tool and the output is:
<img src='/Images/image.png' alt='Nmap'/>

# Scanning
Now to be able to connect to a port, the port must be open and listening
You can just try and establish a TCP connection to the port:
- This gives the system your IP address, details of the porgram used to connect
Now insted, attackers use sneaky manipulation of TCP protocol
### Send a TCP FIN packet:
- Does not make a connection attempy; circumvents firewalls
- If the port is in LISTEN, no reply
- If the port is in CLOSED, responds with RESET
### Send a SYN packet:
- If port is open, responds with SYN/ACK
- Yoy return RESET, no connection

# Scanning: dorking
Dorking - This is the use of advanced Google searches to reveal vunerable websites.
For example:
<img src='/Images/image2.png' alt='dorking'>
The above simply allowed one to search any wordpress site moved using the Duplicator plugin. This help one to reveal directory information, plugin versino, server software, internal structure etc.
[To find out more clikc here] (www.exploit-db.com/google-dorks/)

# Scanning: SHODAN
Device search engine
Permites searching by:
1. IP address
2. Open Port
3. Active protocol
4. Vunerabiliti number

# Enumeratino
The process of identifying easily exploitable vunerabilities and user accounts
Scanning shows you the doors,; enumeration identifies how to get through them safely

Long process, usually involving many automated queries of some kind
An example: Usernames
Target: some system with password-based authentication (most of them)
From your footprinting, you have identified a list of users of your target

So start testing possible username in the logon:
1. Email addresses
2. Known aliases

By inspecting the system's response, you can tell whether a username exists or not

For example: The server returns <img src='/Images/image3.png' alt='invalid username'> if the user does not exist, and **invalid credentials** if it does
Using this, you can build a list of valid usernames

# Enumeration - Spidering
Spidering - This is the automated mapping of a website ir filesystem
A program that recursively follows all lnks in a HTML document
Can expand to include common directory names, hidden files
Can reveal old, insecure pages, backups, unreleased content, databses connected to stite

# Penetration
This is entering the system using information discovered up till now
Once a system is penetrated, the attacket has all the access required t=for their objective

# Attacking
The attacker accomplishes theor goal
Goal could be:
1. Theft of info
2. Denial of service
3. Defacement of web pagesEscalation of priviledge
4. Using one attack to effect another, larger attack

# Covering treacks
Inexperienced attackers leave evidence:
1. Attacker's IP address
2. Attacker's ISP
3. Attacker's Organization
4. Attacker's machine
Professional attackers:
1. Turn off event logging
2. Clear event logs
3. Better for them to knpw that someone was there easily than find out who with little work
4. Hide malicious files that they have left behind
5. Disguise files as other files
6. Hide evil files inside good files

# Installing backdoors
Now one attacker may have done all this hard work, why do it all over again
Install a backdoor to make intrusion easier
Could be:
1. Rogue user account
2. Scheduled Job
3. Hidden program
4. Startup process

<img src='/Images/image4.png'>

# Attacking a webserver
Identify all entry points
+ Entry point - Something you can do to make the server do something
+ Examine the structure of the entry points
- What form does the request take?
- What does it look like each part of it does?
- What happens if you circumvent validation?

Are there non-HTTP ways of communicating witht he server?
+ Text services? Email commands? API?
+ Does the server use another system that you can manipulate or spoof?
+ What does nmap say?
* Identify the server software
* Enumerate, as detailed earlier
* Search for known vunerabilities

So information is all; - The more you know about your system, the more you can defend it. The less the attacker knows the harder it is for them to attack
As admins, you must:
1. Know how the attackers work
2. Know thier tools
3. Know the methodologies